# About

`WaiT` project is a chat project such as whatsapp, telegram and ... with this important difference waiting for Mahdi and Messaieh to arrive and appear and make the world full of peace and love. Therefore we must be get rid of governments such as open source softwares.

# Technical

## Online Users

we get minimum packet size, almost 100 bytes. In one month it reachs to 8.2 MB if we play ping pong with server every 30 seconds.

## channel variable

it's a good idea if we send all connected clients connection pointer to channel variable along side of the jid of a user. for 65000 users, the size of array variable (containing all socket connection addresses) almost is equal to 30 bytes (20 size of jid, a digit in long type+8 size of pointer of connection) 1.85 GB

# Decisions


# Problems

## open file limit

`ulimit -n 200000`

## network limit

https://github.com/golang/go/issues/6785

error `dial tcp 127.0.0.1:9999: connect: cannot assign requested address`
@see MaxIdleConnsPerHost
@see https://paperairoplane.net/?p=556

### examples

#### ping pong

@see https://systembash.com/a-simple-go-tcp-server-and-tcp-client/
@see https://gist.github.com/kenshinx/5796276
@see http://zhen.org/blog/graceful-shutdown-of-go-net-dot-listeners/

### you should read

@see https://golang.org/pkg/bufio/


### solution

@see https://mrotaru.wordpress.com/2013/10/10/scaling-to-12-million-concurrent-connections-how-migratorydata-did-it/

@see https://stackoverflow.com/questions/410616/increasing-the-maximum-number-of-tcp-ip-connections-in-linux

@see https://serverfault.com/questions/168285/max-number-of-socket-on-linux

@see https://www.elastic.co/guide/en/elasticsearch/reference/current/vm-max-map-count.html

@see https://stackoverflow.com/questions/5945695/number-of-network-connections-possible

@see https://serverfault.com/questions/168285/max-number-of-socket-on-linux

@see https://stackoverflow.com/questions/2350071/maximum-number-of-concurrent-connections-on-a-single-port-socket-of-server

# docker

## build

```
docker build --no-cache=true -t emamirazavi/wait_server01 .
```

```
docker run -d --cpus=".05" --name wait_server01 -v /home/bevaght/projects/wait/src/:/app -p 9999:9999 emamirazavi/wait_server01
docker run -d --cpus=".05" --name wait_client01 -v /home/bevaght/projects/wait/src/client/:/app emamirazavi/wait_server01
```
