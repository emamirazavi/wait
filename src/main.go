// https://github.com/avelino/awesome-go
// https://github.com/jinzhu/gorm
// database redis and mysql with percona
// each server must have apis to registration and ...
// https://www.parsgreen.com/
// https://panel.kavenegar.com/Client/Verification/Create
package main

import (
	"bufio"
	"encoding/gob"
	"fmt"
	"log"
	"net"
	"strconv"
	"strings"
)

type wid struct {
	userID string
	con    net.Conn
}

// Message some docs
type Message struct {
	from string
	to   string
	body string
}

// WidsMap some docs
type WidsMap map[string]net.Conn

func server(widChan chan wid, msgChan chan Message, wids WidsMap) {
	defer func() {
		fmt.Printf("Shutting down server")
		for i, conn := range wids {
			if conn != nil {
				fmt.Printf("Closing connection #%s", i)
				conn.Close()
			}
		}
	}()
	// listen on a port
	ln, err := net.Listen("tcp", ":9999")
	defer ln.Close()

	if err != nil {
		fmt.Println(err)
		return
	}
	counter := 1
	for {
		// accept a connection
		c, err := ln.Accept()
		fmt.Println("server: connection " + strconv.Itoa(counter) + " accepted!")
		if err != nil {
			fmt.Println(err)
			continue
		}
		// handle the connection
		go handleServerConnection(c, widChan, msgChan)
		counter++
	}

}

func handleServerConnection(conn net.Conn, widChan chan wid, msgChan chan Message) {
	var authorized = false
	defer conn.Close()
	for {
		// will listen for message to process ending in newline (\n)
		rxMessage, rxError := bufio.NewReader(conn).ReadString('\n')
		if rxError != nil {
			log.Println(rxError)
			return
		} else {
			if len(rxMessage) > 0 {
				rxMessage = rxMessage[:len(rxMessage)-1]
				log.Print("handleServerConnection :: Message Received:", string(rxMessage))
				if !authorized {
					authorized = true
					widChan <- wid{
						userID: rxMessage,
						con:    conn,
					}
				} else {
					// msgto
					if strings.Index(rxMessage, "msgto:") != -1 {
						buff := strings.Split(rxMessage, ":")
						msgChan <- Message{
							from: "anonymous",
							to:   buff[1],
							body: buff[2],
						}
						// rest :=
					}
				}
			}
		}

		// sample process for string received
		// newmessage := strings.ToUpper(message)
		// // send new string back to client
		// conn.Write([]byte(newmessage + "\n"))
	}
}

func handleServerConnection1(c net.Conn, widChan chan wid, msgChan chan Message) { // receive the message
	var msg string
	// wid_str := "mywid"
	var authorized = false
	for {
		err := gob.NewDecoder(c).Decode(&msg)
		if err != nil {
			fmt.Println(err)
		} else {
			log.Println("Received", msg)
			if !authorized {
				authorized = true
				widChan <- wid{
					userID: msg,
					con:    c,
				}
			} else {
				// msgto
				if strings.Index(msg, "msgto:") != -1 {
					buff := strings.Split(msg, ":")
					msgChan <- Message{
						from: "anonymous",
						to:   buff[1],
						body: buff[2],
					}
					// rest :=
				}
			}
		}
	}
	// c.Close()
}

func midman(msgChan chan Message, wids WidsMap) {
	for {
		log.Printf("midman waits for msgChan")
		buff := <-msgChan
		log.Printf("midman found a message %s to %s", buff.body, buff.to)
		// jsonString, _ := json.Marshal(*wids)
		// log.Println("OH! NO! %s", string(jsonString))
		if conn, ok := wids[buff.to]; ok {
			// log.Println("OH! NO!")
			// // send new string back to client
			conn.Write([]byte(buff.body + "\n"))

			// forward to `to` conn
			// w := bufio.NewWriter(conn)
			// w.Write([]byte(buff.body))
			// w.Flush()
			// log.Printf("midman sends %s to %s", buff.body, buff.to)
		}

		// = widBuff.con
		// fmt.Println("user " + widBuff.userID + " connected!")
	}
}

func node(widChan chan wid, wids WidsMap) {
	for {
		widBuff := <-widChan
		wids[widBuff.userID] = widBuff.con
		fmt.Println("user " + widBuff.userID + " connected!")
	}

	// receive message from one goroutine and send it to other goroutine
	// an struct with from and to and message to send

	// wid_str and con must be sent by channel message
	// wids[wid_str] = c
}

func main() {
	// test := "user1\n"
	// fmt.Println(test[:len(test)-1])
	// return
	widChan := make(chan wid)
	msgChan := make(chan Message)
	wids := make(WidsMap)
	go server(widChan, msgChan, wids)
	// 100 6.8 MB
	//
	go node(widChan, wids)
	go midman(msgChan, wids)

	for {
		var input string
		fmt.Scanln(&input)
		if input == "quit" {
			break
		}
	}

}
