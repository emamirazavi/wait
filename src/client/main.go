package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"net"
	"strconv"
	"time"
)

// var conns chan net.Conn = make(chan net.Conn)
const maxClients = 2

func client(userID string) {
	authorized := false
	// timeoutDuration := time.Duration(60)
	// connect to the server
	conn, err := net.Dial("tcp", "127.0.0.1:9999")
	// conn.SetWriteDeadline(time.Second * timeoutDuration)
	// conn.SetReadDeadline(time.Second * time.Duration(1))
	if err != nil {
		fmt.Println(err)
		return
	}

	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)
	// fmt.Print(r1.Intn(100), ",")
	// fmt.Print(r1.Intn(100), ",")
	// return
	// send the message
	// msg := msg
	// listen for reply
	go func() {
		for {
			message, _ := bufio.NewReader(conn).ReadString('\n')
			fmt.Print("Message from server: " + message)
		}
	}()
	for {
		msg := ""
		if !authorized {
			authorized = true
			msg = userID
		} else {
			msg = "msgto:user" + strconv.Itoa(r1.Intn(maxClients)+1) + ":یا مهدی ادرکنی"
		}
		if len(msg) > 1 {
			// send to socket
			fmt.Fprintf(conn, msg+"\n")
			// err = gob.NewEncoder(conn).Encode(msg)
			// if err != nil {
			// 	fmt.Println(err)
			// }
		}
		// read
		// log.Printf(userID + " try to read")

		//
		// buff := make([]byte, 1024)
		// conn.SetReadDeadline(time.Now().Add(timeoutDuration))
		// _, err := conn.Read(buff)
		// if err != nil {
		// 	log.Println("NOTE: %s", err)
		// 	// log.Printf("userID: %s received %s", userID, buff[:n])
		// } else {
		// 	log.Println("OKAY!!!")
		// }
		// conn.SetWriteDeadline(time.Now().Add(timeoutDuration))
		time.Sleep(time.Duration(r1.Intn(10)) + time.Second*time.Duration(r1.Intn(10)))
		// time.Sleep(time.Second * 10)
	}
	// c.Close()
}

func main() {
	for i := 1; i <= maxClients; i++ {
		time.Sleep(time.Millisecond * 5)
		go client("user" + strconv.Itoa(i))
	}

	for {
		var input string
		fmt.Scanln(&input)
		if input == "quit" {
			break
		}
	}
}
