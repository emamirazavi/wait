// https://gobyexample.com/json
// https://blog.golang.org/json-and-go
// https://golang.org/doc/code.html
package parser

import (
	"encoding/json"
	"fmt"
	"os"
)

type Message struct {
	Name string
	Body string
	Time int64
}
